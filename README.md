# NothingMuch

### Overview

A [Polymer](https://www.polymer-project.org/) based app that allows quick search for a GIF.
Uses the [GIPHY API](https://developers.giphy.com/explorer/) to search for GIFs and the
[WordNik API](http://developer.wordnik.com/) to suggest related searches.

### Demo

[View online](https://nothingmuch-mcsnolte.firebaseapp.com/) via Firebase

### Setup

First, install [Bower](https://bower.io/) using [npm](https://www.npmjs.com)

    npm install -g bower

Then install dependencies

    bower install

Finally install [Polymer CLI](https://www.polymer-project.org/2.0/docs/tools/polymer-cli)

    npm install -g polymer-cli

### Start the development server

This command serves the app at `http://127.0.0.1:8081` and provides basic URL
routing for the app:

    polymer serve

### Run tests

This command will run [Web Component Tester](https://github.com/Polymer/web-component-tester)
against the browsers currently installed on your machine:

    polymer test

